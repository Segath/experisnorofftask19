﻿namespace Task19_EntityFramework
{
    partial class EntityFrameworkTut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView_tableView = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_update = new System.Windows.Forms.Button();
            this.btn_create = new System.Windows.Forms.Button();
            this.btn_json = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_studName = new System.Windows.Forms.Label();
            this.lbl_assSupervisor = new System.Windows.Forms.Label();
            this.txtB_studName = new System.Windows.Forms.TextBox();
            this.comB_supervisor = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_tableView)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView_tableView
            // 
            this.dataGridView_tableView.AllowUserToAddRows = false;
            this.dataGridView_tableView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_tableView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_tableView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel1.SetColumnSpan(this.dataGridView_tableView, 2);
            this.dataGridView_tableView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_tableView.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_tableView.Margin = new System.Windows.Forms.Padding(3, 3, 3, 20);
            this.dataGridView_tableView.MultiSelect = false;
            this.dataGridView_tableView.Name = "dataGridView_tableView";
            this.dataGridView_tableView.RowHeadersVisible = false;
            this.dataGridView_tableView.RowHeadersWidth = 62;
            this.dataGridView_tableView.RowTemplate.Height = 28;
            this.dataGridView_tableView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_tableView.Size = new System.Drawing.Size(794, 314);
            this.dataGridView_tableView.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.dataGridView_tableView, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(800, 450);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.btn_delete, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.btn_update, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btn_create, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btn_json, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(403, 340);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(394, 107);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // btn_delete
            // 
            this.btn_delete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_delete.Location = new System.Drawing.Point(200, 56);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(191, 48);
            this.btn_delete.TabIndex = 3;
            this.btn_delete.Text = "Delete";
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this.Btn_delete_Click);
            // 
            // btn_update
            // 
            this.btn_update.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_update.Enabled = false;
            this.btn_update.Location = new System.Drawing.Point(3, 56);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(191, 48);
            this.btn_update.TabIndex = 2;
            this.btn_update.Text = "Update";
            this.btn_update.UseVisualStyleBackColor = true;
            // 
            // btn_create
            // 
            this.btn_create.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_create.Location = new System.Drawing.Point(3, 3);
            this.btn_create.Name = "btn_create";
            this.btn_create.Size = new System.Drawing.Size(191, 47);
            this.btn_create.TabIndex = 1;
            this.btn_create.Text = "Create";
            this.btn_create.UseVisualStyleBackColor = true;
            this.btn_create.Click += new System.EventHandler(this.Btn_create_Click);
            // 
            // btn_json
            // 
            this.btn_json.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_json.Location = new System.Drawing.Point(200, 3);
            this.btn_json.Name = "btn_json";
            this.btn_json.Size = new System.Drawing.Size(191, 47);
            this.btn_json.TabIndex = 0;
            this.btn_json.Text = "Serialize data";
            this.btn_json.UseVisualStyleBackColor = true;
            this.btn_json.Click += new System.EventHandler(this.Btn_json_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.lbl_studName, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lbl_assSupervisor, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtB_studName, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.comB_supervisor, 1, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 340);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(394, 107);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // lbl_studName
            // 
            this.lbl_studName.AutoSize = true;
            this.lbl_studName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_studName.Location = new System.Drawing.Point(3, 0);
            this.lbl_studName.Name = "lbl_studName";
            this.lbl_studName.Size = new System.Drawing.Size(191, 53);
            this.lbl_studName.TabIndex = 0;
            this.lbl_studName.Text = "Student Name";
            // 
            // lbl_assSupervisor
            // 
            this.lbl_assSupervisor.AutoSize = true;
            this.lbl_assSupervisor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_assSupervisor.Location = new System.Drawing.Point(3, 53);
            this.lbl_assSupervisor.Name = "lbl_assSupervisor";
            this.lbl_assSupervisor.Size = new System.Drawing.Size(191, 54);
            this.lbl_assSupervisor.TabIndex = 1;
            this.lbl_assSupervisor.Text = "Assigned Supervisor";
            // 
            // txtB_studName
            // 
            this.txtB_studName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtB_studName.Location = new System.Drawing.Point(200, 3);
            this.txtB_studName.Name = "txtB_studName";
            this.txtB_studName.Size = new System.Drawing.Size(191, 26);
            this.txtB_studName.TabIndex = 2;
            // 
            // comB_supervisor
            // 
            this.comB_supervisor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comB_supervisor.FormattingEnabled = true;
            this.comB_supervisor.Location = new System.Drawing.Point(200, 56);
            this.comB_supervisor.Name = "comB_supervisor";
            this.comB_supervisor.Size = new System.Drawing.Size(191, 28);
            this.comB_supervisor.TabIndex = 3;
            // 
            // EntityFrameworkTut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "EntityFrameworkTut";
            this.Text = "Database viewer";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_tableView)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_tableView;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_update;
        private System.Windows.Forms.Button btn_create;
        private System.Windows.Forms.Button btn_json;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label lbl_studName;
        private System.Windows.Forms.Label lbl_assSupervisor;
        private System.Windows.Forms.TextBox txtB_studName;
        private System.Windows.Forms.ComboBox comB_supervisor;
    }
}

