﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task19_EntityFramework.Model
{
    class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Supervisor Supervisor { get; set; }
        public int SupervisorId { get; set; }
    }
}
