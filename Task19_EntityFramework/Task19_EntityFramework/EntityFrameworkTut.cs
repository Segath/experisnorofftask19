﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;
using Task19_EntityFramework.Model;

namespace Task19_EntityFramework
{
    public partial class EntityFrameworkTut : Form
    {
        private Task19_EntityFrameworkDBContext dbContext;

        public EntityFrameworkTut()
        {
            InitializeComponent();
            dbContext = new Task19_EntityFrameworkDBContext();
            PopulateCombobox();
            RefreshTableView();
        }

        #region Helper Methods

        public void PopulateCombobox()
        {
            comB_supervisor.Items.Clear();
            comB_supervisor.Sorted = true;
            List<Supervisor> supervisors = dbContext.Supervisors.ToList();
            foreach(Supervisor supervisor in supervisors)
            {
                comB_supervisor.Items.Add(supervisor);
            }
        }

        public void RefreshTableView()
        {
            BindingSource tempSource = new BindingSource();
            dataGridView_tableView.DataSource = tempSource;
            tempSource.DataSource = dbContext.Students.ToList();
            dataGridView_tableView.Refresh();
        }

        #endregion

        private void Btn_create_Click(object sender, EventArgs e)
        {
            Student stud = new Model.Student
            {
                Name = txtB_studName.Text,
                SupervisorId = ((Supervisor)comB_supervisor.SelectedItem).Id
            };
            dbContext.Students.Add(stud);
            dbContext.SaveChanges();
            RefreshTableView();
        }

        private void Btn_delete_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = dataGridView_tableView.SelectedRows[0];
            Student tempStud = dbContext.Students.Find(row.Cells[0].Value);
            dbContext.Students.Remove(tempStud);
            dbContext.SaveChanges();
            RefreshTableView();
        }

        private void Btn_json_Click(object sender, EventArgs e)
        {
            using(StreamWriter sw = new StreamWriter("Student.json"))
            {
                sw.Write(JsonConvert.SerializeObject(dbContext.Students.ToList()));
            }
        }
    }
}
