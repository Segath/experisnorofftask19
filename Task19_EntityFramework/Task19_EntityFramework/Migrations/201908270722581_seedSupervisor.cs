namespace Task19_EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seedSupervisor : DbMigration
    {
        public override void Up()
        {

            Sql("INSERT INTO Supervisors (Name) VALUES ('Tom')");
            Sql("INSERT INTO Supervisors (Name) VALUES ('Jerry')");
            Sql("INSERT INTO Supervisors (Name) VALUES ('Bob')");

        }
        
        public override void Down()
        {
            Sql("DELETE FROM Supervisors WHERE Name = 'Tom'");
            Sql("DELETE FROM Supervisors WHERE Name = 'Jerry'");
            Sql("DELETE FROM Supervisors WHERE Name = 'Bob'");
        }
    }
}
